//
//  GPSTester.swift
//  iBeacon Fingerprint Localization
//
//  Created by Elmer Orellana on 5/22/17.
//  Copyright © 2017 RouteMe2. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
import AVFoundation

class SensorFusion: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {
    /************************************* Var and Labeles ****************************************************/
    let startLatitude = 36.9999510000            // This is the latitude at the origin
    let startLongitude = -122.0623370000         // This is the longitude at the origin
    let kNearest = 4                             // selects the number of nearest neighbors
    let iBeaconMajor:NSNumber = 9073             // All beacons share the same
    let iBeaconMinor_1:NSNumber = 1              // Each beacon has a
    let iBeaconMinor_2:NSNumber = 2              // unique minor
    let iBeaconMinor_3:NSNumber = 3
    var iBeaconRSSI_1 = 0.0                      // RSSI from iBeacon 1
    var iBeaconRSSI_2 = 0.0                      // RSSI from iBeacon 2
    var iBeaconRSSI_3 = 0.0                      // RSSI from iBeacon 3
    var signatureTable = [[Double]]()            // Will hold the entire database
    var signatureArray = [[Double]]()            // Will hold the RP vector
    var signatureArrayAverage = [Double]()       // Will hold the averaged out RP vector
    var GPSLatitude = 0.0                        // Will hold the latitude of eu
    var GPSLongitude = 0.0                       // Will hold the longitude of eu
    var GPSAccuracy = 0.0                        // Will hold the GPS horizontal accuracy
    var timer = Timer()                          // Create instance of timer
    var writeString:String = ""                  // Will hold the string that will be written to file
    var RSSI_1 = 0.0                             // Will hold the average
    var RSSI_2 = 0.0                             // of the RSSIs collected
    var RSSI_3 = 0.0                             // by the collectSignatures button
    var GPSReportName = ""                       // This will be the string that hold the formatted
                                                 // report name
    var RSSISum = 0.0                            // Sum of real-time RSSI to calculate the accuracy
    var coordinates = [0.0,0.0]                  // will store the approximated coordinates of the eu
    var circleColor:UIColor = UIColor.clear      // Circle color
    var fillColor:UIColor = UIColor.clear        // Circle fill color
    var fingerprintLatitude = 0.0                // holds the latitude coordinate from the
                                                 //fingerprint algorithm
    var fingerprintLongitude = 0.0               // holds the longitue coordinate from the
                                                 //fingerprint algorithm
    var fingerprintAccuracy = 0.0                // holds the accuracy of the fingerprint localization
                                                 //coordinates
    var fusedLatitude = 0.0                      // Latitude from sensor fusion
    var fusedLongitude = 0.0                     // Longitude from sensor fusion
    var fusedAccuracy = 0.0                      // Accuracy of sensor fusion
    var cartesianX = 0.0                         // Will hold the fingerprint x component
    var cartesianY = 0.0                         // Will hold the fingerprint y component
    let mapZoomLevel = 0.0001                    // This is the map span level    
    // JSON parameters
    var busStopName:String = "unknown"
    var transitName:String = "none"
    var nearest:String = "true"
    var mode:String = "unknown"
    var tripInstanceID:String = "7564153e33aadd0ba033a9ee7b624a89"
    var travelerID:String = "50965d1fa6e7625057c1615418819dc1b9f89139924646397b43969c9ac27a17"
    
    // State Machine Parameters
    enum State {
        case noState, start, noBus, correctBusStop, incorrectBusStop, boardingZone, seeBus, onBus, atDestination
    }
    
    let RSSISumThreshold = 240.0
    let stopProximityThreshold = -70.0
    var currentState = State.start
    var lastState = State.noState
    var stateFlag = 0
    let repeatNumber = 2
    var detectedIbeacon = CLBeacon()
    
    // Audio Players ---------------------------------------------------------
    
    var walkToStopPlayer: AVAudioPlayer = AVAudioPlayer()
    var correctBusStopPlayer: AVAudioPlayer = AVAudioPlayer()
    var waitToBoardPlayer: AVAudioPlayer = AVAudioPlayer()
    var incorrectBusStopPlayer: AVAudioPlayer = AVAudioPlayer()
    var onRoutePlayer: AVAudioPlayer = AVAudioPlayer()
    var busIsHerePlayer: AVAudioPlayer = AVAudioPlayer()
    var busDestinationPlayer: AVAudioPlayer = AVAudioPlayer()
    var fsmErrorPlayer: AVAudioPlayer = AVAudioPlayer()
    
    
    
    // Declare a location Manager that coordinates all locations
    let locationManager = CLLocationManager()
    
    @IBOutlet weak var map: MKMapView!
    
    // Select region to monitor our own iBeacons from kontakt.io
    // by using our UUID and identifing them as kontakt beacons
    // For monitoring a specific iBeacon include that iBeacon's major and minor
    
    let regionToMonitor = CLBeaconRegion(proximityUUID: UUID(uuidString:"F7826DA6-4FA2-4E98-8024-BC5B71E0893E")!, identifier: "kontakt beacons")
    
    
    // Globle variable for file's name that will contain the data collected
    var databaseName:String = "fingerprintDbiPhone"

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //////////////// locationManager delegate declaration and request for location authorization ///////////////
        
        
        // Let this class receive location updates
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        
        // Request authorization to use location services. Manually done in iOS settings.
        if (CLLocationManager.authorizationStatus() != CLAuthorizationStatus.authorizedWhenInUse){
            locationManager.requestWhenInUseAuthorization()
        }
        
        // Begin monitoring
        locationManager.startRangingBeacons(in: regionToMonitor)
        locationManager.startUpdatingLocation()
        
        signatureTable = parseDatabaseFile()    // Parses the file to break down the database file

        /*****************************************************************************************/
        
        //Create a file name that is unique
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "MMddyyyy-hhmmss"
        let dateStringFormat = dayTimePeriodFormatter.string(from: Date())
        
        GPSReportName = "GPSReport\(dateStringFormat).txt"
        
        //////////////// locationManager delegate declaration and request for location authorization ///////////////
        
        
        // Let this class receive location updates
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        // Request authorization to use location services. Manually done in iOS settings.
        if (CLLocationManager.authorizationStatus() != CLAuthorizationStatus.authorizedWhenInUse){
            locationManager.requestWhenInUseAuthorization()
        }
        
        // Initializing audio files to their players
         do {
         let walkToStopPath = Bundle.main.path(forResource: "walkToStop", ofType: "aiff")
         let correctBusStopPath = Bundle.main.path(forResource: "correctBusStop", ofType: "aiff")
         let waitToBoardPath = Bundle.main.path(forResource: "waitToBoard", ofType: "aiff")
         let incorrectBusStopPath = Bundle.main.path(forResource: "incorrectBusStop", ofType: "aiff")
         let onRoutePath = Bundle.main.path(forResource: "onRoute", ofType: "aiff")
         let busIsHerePath = Bundle.main.path(forResource: "yourBusIsHere", ofType: "aiff")
         let busDestinationPath = Bundle.main.path(forResource: "destination", ofType: "aiff")
         let fsmErrorPath = Bundle.main.path(forResource: "fsmError", ofType: "aiff")
         
         try walkToStopPlayer = AVAudioPlayer(contentsOf: NSURL(fileURLWithPath: walkToStopPath!) as URL)
         try correctBusStopPlayer = AVAudioPlayer(contentsOf: NSURL(fileURLWithPath: correctBusStopPath!) as URL)
         try waitToBoardPlayer = AVAudioPlayer(contentsOf: NSURL(fileURLWithPath: waitToBoardPath!) as URL)
         try incorrectBusStopPlayer = AVAudioPlayer(contentsOf: NSURL(fileURLWithPath: incorrectBusStopPath!) as URL)
         try onRoutePlayer = AVAudioPlayer(contentsOf: NSURL(fileURLWithPath: onRoutePath!) as URL)
         try busIsHerePlayer = AVAudioPlayer(contentsOf: NSURL(fileURLWithPath: busIsHerePath!) as URL)
         try busDestinationPlayer = AVAudioPlayer(contentsOf: NSURL(fileURLWithPath: busDestinationPath!) as URL)
         try fsmErrorPlayer = AVAudioPlayer(contentsOf: NSURL(fileURLWithPath: fsmErrorPath!) as URL)
         
         }catch{
         
         // error
         print("error: Audio files didn't match their players")
         }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location = locations[0]
        
        // Get the latitude and longitude of eu
        GPSLatitude = location.coordinate.latitude
        GPSLongitude = location.coordinate.longitude
        GPSAccuracy = location.horizontalAccuracy
        
    }

    
    func mapView(_ mapView: MKMapView!, rendererFor overlay: MKOverlay!) -> MKOverlayRenderer! {
        if overlay is MKCircle {
            let circle = MKCircleRenderer(overlay: overlay)
            circle.strokeColor = circleColor
            circle.fillColor = fillColor
            circle.lineWidth = 1
            
            
            return circle
        } else {
            return nil
        }
    }


    
    // Receive iBeacon updates on their current informatiom
    func locationManager(_ manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], in region: CLBeaconRegion) {
        
        var southBusStopRSSI = -100.00
        var zIDY = CLBeacon()
        var mNZR = CLBeacon()
        var d2R2 = CLBeacon()
        var pYEM = CLBeacon()
        
        
        // Grabs iBeacons' information
        for beacon in beacons {
            if beacon.major == iBeaconMajor {
                if beacon.minor == iBeaconMinor_1{
                    iBeaconRSSI_1 = Double(beacon.rssi)
                    if iBeaconRSSI_1 == 0.0{
                        iBeaconRSSI_1 = -100
                    }
                }
            }
            
            if beacon.major == iBeaconMajor {
                if beacon.minor == iBeaconMinor_2{
                    iBeaconRSSI_2 = Double(beacon.rssi)
                    if iBeaconRSSI_2 == 0.0{
                        iBeaconRSSI_2 = -100
                    }
                }
            }
            
            if beacon.major == iBeaconMajor {
                if beacon.minor == iBeaconMinor_3{
                    iBeaconRSSI_3 = Double(beacon.rssi)
                    if iBeaconRSSI_3 == 0.0{
                        iBeaconRSSI_3 = -100
                    }
                }
            }
            
            if beacon.minor == 10655{
                southBusStopRSSI = Double(beacon.rssi)
            }
            
            if beacon.major == 8444 && beacon.minor == 59182{
                zIDY = beacon
            }
            
            if beacon.major == 64013 && beacon.minor == 18457{
                mNZR = beacon
            }
            
            // not 100% sure if the major is correct on D2R2
            if beacon.major == 20078 && beacon.minor == 48048{
                d2R2 = beacon
            }
            
            if beacon.major == 3 && beacon.minor == 59083{
                pYEM = beacon
            }
            
            detectedIbeacon = beacon
        }
        
        // Call the find my location function
        findMyLocation()
        
        // Set the span of the map location
        
        let span:MKCoordinateSpan = MKCoordinateSpanMake(mapZoomLevel, mapZoomLevel)
        
        // Predefined location for GPS
        let myGPSLocation = CLLocationCoordinate2D(latitude: GPSLatitude, longitude: GPSLongitude)
        
        // Custom circle for GPS
        circleColor = UIColor.blue
        fillColor = UIColor(red: 0, green: 0, blue: 255, alpha: 0.1)
        //addRadiusCircle(location: myGPSLocation, radius: GPSAccuracy)
        self.map.delegate = self
        var circle = MKCircle(center: myGPSLocation, radius: GPSAccuracy as CLLocationDistance)
        self.map.add(circle)
        
        
        
        // Predefined location for fingerprinting
        let myFingerprintLocation = CLLocationCoordinate2D(latitude: fingerprintLatitude, longitude: fingerprintLongitude)
        
        // Custom circle for fingerprinting
        circleColor = UIColor.red
        fillColor = UIColor(red: 255, green: 0, blue: 0, alpha: 0.1)
        //addRadiusCircle(location: myFingerprintLocation, radius: fingerprintAccuracy)
        self.map.delegate = self
        circle = MKCircle(center: myFingerprintLocation, radius: fingerprintAccuracy as CLLocationDistance)
        self.map.add(circle)
        
        
        /*********************************** Define fingerprint accuracy *****************************************/
        // Compute the RSSI sum to calculate the fingerprint accuracy
        RSSISum = abs(iBeaconRSSI_1 + iBeaconRSSI_2 + iBeaconRSSI_3)
        
        print("RSSIsum: \(RSSISum)")
        
        if (RSSISum >= 258.0){
            fingerprintAccuracy = 0*RSSISum + 6.839999999999899
        }else if (RSSISum > 239.0){
            fingerprintAccuracy = 0.035237867954*RSSISum - 2.24485586103
        }else if (RSSISum > 220.0){
            fingerprintAccuracy = 0.128836566109*RSSISum - 24.5433842661
        }else if (RSSISum > 201.0){
            fingerprintAccuracy = 0.0564741573133*RSSISum - 8.88146057467
        }else if (RSSISum > 182.0){
            fingerprintAccuracy = 0.0801475319747*RSSISum - 14.0348725814
        }
        
        /*********************************** Sensor fusion starts here *****************************************/
        
        let GPSWeightComponent = 1.0/GPSAccuracy
        let fingerprintWeightComponent = 1.0/fingerprintAccuracy
        
        let totalLocationWeight = GPSWeightComponent + fingerprintWeightComponent
        
        // Normalize weight
        let GPSWeight = GPSWeightComponent / totalLocationWeight
        let fingerprintWeight = fingerprintWeightComponent / totalLocationWeight
        
        fusedLatitude  = GPSWeight * GPSLatitude  + fingerprintWeight * fingerprintLatitude
        fusedLongitude = GPSWeight * GPSLongitude + fingerprintWeight * fingerprintLongitude
        fusedAccuracy  = GPSWeight * GPSAccuracy  + fingerprintWeight * fingerprintAccuracy
        
        /************************************ Sensor fusion ends here *****************************************/
        
        
        // Predefined location for sensor fusion
        let myFusedLocation:CLLocationCoordinate2D = CLLocationCoordinate2DMake(fusedLatitude as CLLocationDegrees, fusedLongitude as CLLocationDegrees)
        
        print("Latitude: \(fusedLatitude), Longitude: \(fusedLongitude)")
        
        let region:MKCoordinateRegion = MKCoordinateRegionMake(myFusedLocation, span)
        
        map.setRegion(region, animated: true)
        
        // Custom circle for fingerprinting
        circleColor = UIColor.purple
        fillColor = UIColor(red: 255, green: 0, blue: 255, alpha: 0.1)
        self.map.delegate = self
        circle = MKCircle(center: myFusedLocation, radius: fusedAccuracy as CLLocationDistance)
        self.map.add(circle)
        
        // Clear fused latitude and longitude
        fusedLatitude  = 0.0
        fusedLongitude = 0.0
        fusedAccuracy  = 0.0
        
        // Show location on map
        self.map.showsUserLocation = false
        
        
        /*********************** Bus stop state machine **********************************/
        // the estimated GPS coordinate of the spot to wait for the bus is (36.99990, -122.06235)
        // the latitude threshold delta is .000057
        let boardingLatitude = 36.99990
        let boardingLongitude = -122.06235
        let latitudeThresholdDelta = 0.000057
        let longitudeThresholdDelta = -0.000085
        let latitudeLowerBound = boardingLatitude - latitudeThresholdDelta
        let latitudeUpperBound = boardingLatitude + latitudeThresholdDelta
        let longitudeLowerBound = boardingLongitude - longitudeThresholdDelta
        let longitudeUpperBound = boardingLongitude + longitudeThresholdDelta
        
        switch currentState {
        case .start:
            
            if stateFlag <= repeatNumber{
                walkToStopPlayer.play()
            }
            
            if lastState == currentState{
                stateFlag += 1
            }else{
                stateFlag = 0
            }
            
            lastState = currentState
            
            if RSSISum < RSSISumThreshold{
                currentState = .correctBusStop
            }
            
            if (southBusStopRSSI >= -70.00){
                
                //busStopName = "south_science_hill_busstop"
                
                currentState = .incorrectBusStop
            }
            
        case .correctBusStop:
            
            if stateFlag <= repeatNumber{
                correctBusStopPlayer.play()
            }
            
            if lastState == currentState{
                stateFlag += 1
            }else{
                stateFlag = 0
            }
            
            lastState = currentState
            
            if ((fusedLatitude > latitudeLowerBound && fusedLatitude < latitudeUpperBound) && (fusedLongitude > longitudeLowerBound && fusedLongitude < longitudeUpperBound)){
                currentState = .boardingZone
            }
            
            if RSSISum > RSSISumThreshold {
                currentState = .start
            }
            
        case .incorrectBusStop:
            
            if stateFlag <= repeatNumber{
                incorrectBusStopPlayer.play()
            }
            
            if lastState == currentState{
                stateFlag += 1
            }else{
                stateFlag = 0
            }
            
            lastState = currentState
            
            if (southBusStopRSSI < -70.0){
                currentState = .start
            }
            
        case .boardingZone:
            
            if stateFlag <= repeatNumber{
                waitToBoardPlayer.play()
            }
            
            if lastState == currentState{
                stateFlag += 1
            }else{
                stateFlag = 0
            }
            
            lastState = currentState
            
            if (Double(zIDY.rssi) > -90.0 || Double(mNZR.rssi) > -90.0 || Double(d2R2.rssi) > -90.0 || Double(pYEM.rssi) > -90.0){
                currentState = .seeBus
            }
            
            if (!(fusedLatitude > latitudeLowerBound && fusedLatitude < latitudeUpperBound) && (fusedLongitude > longitudeLowerBound && fusedLongitude < longitudeUpperBound)) {
                currentState = .start
            }
            
        case .seeBus:
            
            if stateFlag <= repeatNumber{
                busIsHerePlayer.play()
            }
            
            if lastState == currentState{
                stateFlag += 1
            }else{
                stateFlag = 0
            }
            
            lastState = currentState
            
            if ((Double(zIDY.rssi) > -90.0 || Double(mNZR.rssi) > -90.0 || Double(d2R2.rssi) > -90.0 || Double(pYEM.rssi) > -90.0) && RSSISum > 300){
                currentState = .onBus
            }else{
                currentState = .boardingZone
            }
            
        case .onBus:
            
            if stateFlag <= repeatNumber{
                onRoutePlayer.play()
            }
            
            if lastState == currentState{
                stateFlag += 1
            }else{
                stateFlag = 0
            }
            
            lastState = currentState
            
            if (detectedIbeacon.minor != 1 && detectedIbeacon.minor != 2 && detectedIbeacon.minor != 3 && detectedIbeacon.minor != 59182 && detectedIbeacon.minor != 18457 && detectedIbeacon.minor != 59083 && detectedIbeacon.minor != 48048){
                currentState = .atDestination
            }
            
        case .atDestination:
            
            if stateFlag <= repeatNumber{
                busDestinationPlayer.play()
            }
            
        default:
            
            fsmErrorPlayer.play()
            currentState = .start
        }
        
        
        
        
        /************************** JSON Stuff *******************************************/
        let json: [String: Any] = ["traveler_id":travelerID,"trip_instance_id":tripInstanceID,"latitude":(locationManager.location?.coordinate.latitude)!,"longitude": (locationManager.location?.coordinate.longitude)!,"accuracy_dist":"\(fusedAccuracy)","mode":mode, "nearest":nearest,"stop_name":busStopName,"transit_name":transitName]
        
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        
        //create post request
        
        //let url = URL(string: "https://routeme2app.mybluemix.net/api/check_location")!
        let url = URL(string: "https://requestb.in/xd6ilmxd")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let originalText:String = "testing@aol.com:testing"
        let encoded = originalText.toBase64()
        
        //let encodedText = originalText.data(using: .utf8)!
        request.setValue("Basic \(encoded)", forHTTPHeaderField: "authorization")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        // insert json data to the request
        request.httpBody = jsonData
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            
            guard let data = data, error == nil else {
                
                print(error?.localizedDescription ?? "No data")
                
                return
                
            }
            
            let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
            
            if let responseJSON = responseJSON as? [String: Any] {
                
                print(responseJSON)
            }
        }
        
        task.resume()
    }
    
    // This function calculates the approximated coordinates of the eu
    func findMyLocation(){
        
        // Create an array that holds the differences between the real-time signature RSSI values
        // and the RSSI values of each RP in the database
        var rssiDifferencesArray = compareSignatures(array: signatureTable)
        
        
        var preCoordinates = [0.0,0.0]
        // Clear coordinates
        coordinates = [0.0,0.0]
        
        
        
        // Create a new array with the sum of the 0, 1, and 2 elements of the rssiDifferenceArray
        // append the angle and radius to index 3 and 4 of the
        var differencesSumArray = [[Double]]()
        
        // Calculate the sum of the differences of elements 0, 1, 2, and 3 in the rssiDifferenceArray
        for i in 0..<rssiDifferencesArray.count{
            // Add a row to the differenceSumArray
            differencesSumArray.append([])
            
            // Calculate the sum of the 0, 1, 2 elements of the rssiDifferenceArray
            let diffSum = rssiDifferencesArray[i][0] + rssiDifferencesArray[i][1] + rssiDifferencesArray[i][2]
            
            // append the difference, angle, and radius to the ith element in the differencesSumArray
            differencesSumArray[i] = [diffSum, rssiDifferencesArray[i][3], rssiDifferencesArray[i][4]]
        }
        
        //sort the differencesSumArray in ascending order
        differencesSumArray.sort{($0[0] < $1[0])}
        
        if differencesSumArray[0][0] == 0.0{
            preCoordinates[0] = differencesSumArray[0][1]
            preCoordinates[1] = differencesSumArray[0][2]
        }else{
            // Calculate the weighted average of the location
            
            var rpWeight = [Double]()
            
            for i in 0..<kNearest{
                
                // calculate individual weights and store in rpWeight array
                let weightCalculation = 1/differencesSumArray[i][0]
                rpWeight.append(weightCalculation)
            }
            
            // Calculate the total weight
            var totalWeight = 0.0
            for i in 0..<rpWeight.count{
                totalWeight += rpWeight[i]
            }
            
            // Calculate the weight percentage of the k-nearest RPs
            var weightPercent = [Double]()
            for i in 0..<rpWeight.count{
                weightPercent.append((rpWeight[i]/totalWeight))
            }
            
            //clear coordinates
            preCoordinates = [0.0,0.0]
            
            // Calculate the weighted average of the RP coordinates in cartesian
            for i in 0..<weightPercent.count{
//                print(differencesSumArray[i][1])
                
                cartesianY += weightPercent[i]*differencesSumArray[i][2]*sin((Double.pi/180.0)*(differencesSumArray[i][1] + 26.0))
//                                print("Weight percent: \(weightPercent[i])")
//                                print("Sine Angle: \(sinAngleCalculation)")
                
                cartesianX += weightPercent[i]*differencesSumArray[i][2]*cos((Double.pi/180.0)*(differencesSumArray[i][1] + 26.0))
//                                print("Cosine Angle: \(sinAngleCalculation)")
                
                preCoordinates[1] += weightPercent[i]*differencesSumArray[i][2]
//                                print("arctan: \(atan2(sinAngleCalculation, cosAngleCalculation))")
            }
            if cartesianY > 0.0 && cartesianX > 0.0{
                preCoordinates[0] = (180.0/Double.pi)*atan2(cartesianY, cartesianX)
            }else if cartesianX < 0.0{
                preCoordinates[0] = (180.0/Double.pi)*atan2(cartesianY, cartesianX)
            }else if cartesianY < 0.0 && cartesianX > 0.0{
                preCoordinates[0] = (180.0/Double.pi)*atan2(cartesianY, cartesianX) + 360
            }
        }
        
        coordinates = [preCoordinates[0], preCoordinates[1]]
        
        // Convert the cartesian x and y coordinates to latitude and longitude
        fingerprintLatitude = convertToGPSCoordinates(dx: cartesianX, dy: cartesianY, originLatitude: startLatitude, originLongitude: startLongitude).latitude
        
        fingerprintLongitude = convertToGPSCoordinates(dx: cartesianX, dy: cartesianY, originLatitude: startLatitude, originLongitude: startLongitude).longitude
        
        cartesianX = 0.0
        cartesianY = 0.0
        
        print("Coordinates: (\(coordinates[0]), \(coordinates[1]))")
    }
    
    // This function takes the difference of each element of the database against the real-time
    // signature. It returns a new 2D array of the absolute value of the differences and
    // their corresponding coordinates
    func compareSignatures(array: ([[Double]]))->([[Double]]){
        
        var returnArray = [[Double]]()
        
        // take the difference between the real-time signature and the RSSI values in each RP in
        // the database
        for i in 0..<array.count{
            returnArray.append([])
            
            returnArray[i].append(abs(array[i][0] - iBeaconRSSI_1))        // Beacon 1 RSSI difference
            returnArray[i].append(abs(array[i][1] - iBeaconRSSI_2))        // Beacon 2 RSSI difference
            returnArray[i].append(abs(array[i][2] - iBeaconRSSI_3))        // Beacon 3 RSSI difference
            returnArray[i].append(array[i][3])                             // Angle
            returnArray[i].append(array[i][4])                             // Radius
            //returnArray[i].append(array[i][6])                           // Latitude
            //returnArray[i].append(array[i][7])                           // Longitude
        }
        
        return returnArray
    }

    // This function parses the database file and converts it into a 2D array of doubles containing
    // the elements of each RP vector
    func parseDatabaseFile()->([[Double]]){
        
        //////////////////// Read from database file //////////////////////////////////////////////
        
//         print("trying to read file")
        
        let fileURLProject = Bundle.main.path(forResource: databaseName, ofType: "txt")
        
//         print("read fileName")
        
        var vectorTableStr = [[String]]()
        var vectorTableDouble = [[Double]]()
        
        do {
            let readString = try String(contentsOfFile: fileURLProject!, encoding: String.Encoding.utf8)
            
//            print("about to separate by newline")
            
            let signaturesArray = readString.components(separatedBy: "\n")
            
//            print(signaturesArray)
            
//            print("about to separate by comma")
            
//            print("signatureArray size is: \(signaturesArray.count)")
            
            for i in 0..<signaturesArray.count-1{
                
//                print(i)
                
                vectorTableStr.append([])
                vectorTableDouble.append([])
                
                var signatureVector = signaturesArray[i].components(separatedBy: ",")
                //print("SignatureVector: \(signatureVector)")
                
                // Remove the \r end-of-line string
                signatureVector[signatureVector.count-1] = signatureVector[signatureVector.count-1].replacingOccurrences(of: "\r", with: "")
                
                vectorTableStr[i] = signatureVector
                
//                print("vectorTableStr: \(vectorTableStr)")
                
                // Number of 0.0 represent the size of the RP vectors
                if signatureVector.count == 6{
                    vectorTableDouble[i] = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
                }else{
                    vectorTableDouble[i] = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
                }
//                print("SignatureVector size: \(signatureVector.count)")
                
                for j in 0..<signatureVector.count{
//                     print(j)
                    vectorTableDouble[i][j] = Double(vectorTableStr[i][j])!
//                       print(vectorTableDouble)
                }
            }
            
        }catch let error as NSError{
            print("Failed to read from project")
            print(error)
        }
        
        
        return vectorTableDouble
        /*****************************************************************************************/
    }
    
    // This function converts cartesian coordinates to latitude and longitude
    // It takes as parameters the x and y coordinates, and the origin's known latitude and longitude of
    // the cartesian plane
    // It returns the converted latitude and longitude
    func convertToGPSCoordinates(dx: Double, dy: Double, originLatitude: Double, originLongitude:Double) -> (latitude: Double, longitude: Double){
        
        let deltaLongitude = dx/(111320*cos(originLatitude))
        
        let deltaLatitude = dy/110540
        
        let convertedLongitude = originLongitude + deltaLongitude
        
        let convertedLatitude = originLatitude + deltaLatitude
        
        
        return (convertedLatitude, convertedLongitude)
    }
}

extension String {
    
    /// Encode a String to Base64
    
    func toBase64() -> String {
        
        return Data(self.utf8).base64EncodedString()
        
    }
    
    /// Decode a String from Base64. Returns nil if unsuccessful.
    
    func fromBase64() -> String? {
        
        guard let data = Data(base64Encoded: self) else { return nil }
        
        return String(data: data, encoding: .utf8)
        
    }
}
