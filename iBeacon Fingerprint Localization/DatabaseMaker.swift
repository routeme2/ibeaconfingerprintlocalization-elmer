//
//  FirstViewController.swift
//  iBeacon Fingerprint Localization
//
//  Created by Elmer Orellana on 4/25/17.
//  Copyright © 2017 RouteMe2. All rights reserved.
//

import UIKit
import CoreLocation
import Foundation


class DatabaseMaker: UIViewController, CLLocationManagerDelegate{
    /************************************* Var and Labeles ****************************************************/
    let iBeaconMajor:NSNumber = 9073            // All beacons share the same
    let iBeaconMinor_1:NSNumber = 1             // Each beacon has a
    let iBeaconMinor_2:NSNumber = 2             // unique minor
    let iBeaconMinor_3:NSNumber = 3
    var iBeaconRSSI_1 = 0.0                     // RSSI from iBeacon 1
    var iBeaconRSSI_2 = 0.0                     // RSSI from iBeacon 2
    var iBeaconRSSI_3 = 0.0                     // RSSI from iBeacon 3
    var signatureArray = [[Double]]()           // Will hold the RP vector
    var signatureArrayAverage = [Double]()      // Will hold the averaged out RP vector
    var myLatitude = 0.0                        // Will hold the latitude of eu
    var myLongitude = 0.0                       // Will hold the longitude of eu
    var timer = Timer()                         // Create instance of timer
    var writeString:String = ""                 // Will hold the string that will be written to file
    var RSSI_1 = 0.0                            // Will hold the average
    var RSSI_2 = 0.0                            // of the RSSIs collected
    var RSSI_3 = 0.0                            // by the collectSignatures button
    
    
    
    
    // Select region to monitor our own iBeacons from kontakt.io
    // by using our UUID and identifing them as kontakt beacons
    // For monitoring a specific iBeacon include that iBeacon's major and minor
    
    let regionToMonitor = CLBeaconRegion(proximityUUID: UUID(uuidString:"F7826DA6-4FA2-4E98-8024-BC5B71E0893E")!, identifier: "kontakt beacons")
    
    // Declare a location Manager that coordinates all locations
    let locationManager = CLLocationManager()
    
    
    // Globle variable for file's name that will contain the data collected
    var filename:String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
                //////////////// locationManager delegate declaration and request for location authorization ///////////////
        
        
        // Let this class receive location updates
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        // Request authorization to use location services. Manually done in iOS settings.
        if (CLLocationManager.authorizationStatus() != CLAuthorizationStatus.authorizedWhenInUse){
            locationManager.requestWhenInUseAuthorization()
        }
        
        // Begin monitoring
        locationManager.startRangingBeacons(in: regionToMonitor)
        locationManager.startUpdatingLocation()
        
        //////////////////////////////
        
        ////////////////////////////// Create a unique file name ///////////////////////////////////////////////////
        //Create a file name that is unique
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "MMddyyyy-hhmmss"
        let dateStringFormat = dayTimePeriodFormatter.string(from: Date())

        
        filename = "WBScienceHillDatabase\(dateStringFormat).txt"
        
        ///////////////////////////////// Tap anywhere to hide keyboard ////////////////////////////////////////////
        
        //Looks for single or multiple taps.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(DatabaseMaker.dismissKeyboard))
        
        //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
        tap.cancelsTouchesInView = false
        
        view.addGestureRecognizer(tap)
    }
    
    //Calls this function when the tap is recognized.
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    /*********************************** Location Manager Function ********************************************/
    
    // Receive iBeacon updates on their current informatiom
    func locationManager(_ manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], in region: CLBeaconRegion) {
        
        
        // Grabs iBeacons' information
        for beacon in beacons {
            if beacon.major == iBeaconMajor {
                if beacon.minor == iBeaconMinor_1{
                    if (Double(beacon.rssi) == 0.0){
                        iBeaconRSSI_1 = 100.0
                    }else{
                        iBeaconRSSI_1 = Double(beacon.rssi)
                    }
                }
            }
            
            if beacon.major == iBeaconMajor {
                if beacon.minor == iBeaconMinor_2{
                    if (Double(beacon.rssi) == 0.0){
                        iBeaconRSSI_2 = 100.0
                    }else{
                        iBeaconRSSI_2 = Double(beacon.rssi)
                    }
                }
            }
            
            if beacon.major == iBeaconMajor {
                if beacon.minor == iBeaconMinor_3{
                    if (Double(beacon.rssi) == 0.0){
                        iBeaconRSSI_3 = 100.0
                    }else{
                        iBeaconRSSI_3 = Double(beacon.rssi)
                    }
                }
            }
        }
        
        //Display Beacon signature on iPhone screen
        iBeaconSignature.text = "[\(iBeaconRSSI_1), \(iBeaconRSSI_2), \(iBeaconRSSI_3),\n \(myLatitude),\n \(myLongitude)]"
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations[0]
        
        // Get the latitude and longitude of eu
        myLatitude = location.coordinate.latitude
        myLongitude = location.coordinate.longitude
    }
/**********************************************************************************************************/
    
    @IBOutlet weak var iBeaconSignature: UILabel!
    
    @IBOutlet weak var angleTextField: UITextField!
    
    @IBOutlet weak var radiusTextField: UITextField!
    
    @IBOutlet weak var offSetTextField: UITextField!
    
    @IBOutlet weak var timerTextField: UITextField!
    
    @IBAction func collectSignaturesButton(_ sender: Any) {
        
        // Configure timer
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(DatabaseMaker.collectSignaturesHelper), userInfo: nil, repeats: true)
    }
    
    @IBOutlet weak var timerLabel: UILabel!
    
    @IBAction func CreateRpButton(_ sender: Any) {
        
        // Prepare the information that will be written in text file
        writeString = writeString + "\(RSSI_1),\(RSSI_2),\(RSSI_3),\(angleTextField.text!),\(radiusTextField.text!),\(offSetTextField.text!),\(myLatitude),\(myLongitude)\n"
        
        // Clear the RSSI_x variables
        RSSI_1 = 0.0
        RSSI_2 = 0.0
        RSSI_3 = 0.0
        
        // Reports back to the tester immediate
        timerLabel.text = "Reference point created"
        
        //print(writeString)
        
    }
    
    @IBAction func createDatabaseButton(_ sender: Any) {
        
        // Stop looking for iBeacons
        locationManager.stopRangingBeacons(in: regionToMonitor)
        
        // Stop updating locations
        locationManager.stopUpdatingLocation()
        
        //Locate the document directory and save the data there
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            
            let dirFilePath = dir.appendingPathComponent(filename)
            
            //Save the file
            do {
                try writeString.write(to: dirFilePath, atomically: false, encoding: String.Encoding.utf8)
                
                // Reports back to the tester immediate
                iBeaconSignature.text = "Database Created"
                
            } catch _ {
                
                // Reports back to the tester immediate
                iBeaconSignature.text = "Database Creation Failed"
                
                print("Could not save the file.")
            }
        }
        
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    var globalCounter = 0
    func collectSignaturesHelper(){

        signatureArray.append([])
        
        if globalCounter < Int(timerTextField.text!)!{
            
            // Display counter on the screen
            timerLabel.text = "\(globalCounter+1)"
            
            // Add signature vectors to signatureArray
            signatureArray[globalCounter] = [iBeaconRSSI_1, iBeaconRSSI_2, iBeaconRSSI_3]
            
            globalCounter += 1
            print(signatureArray)
            
        }else{
            
            // Stop timer and clear globalCounter
            timer.invalidate()
            globalCounter = 0
            
            // Calculate the average of the RSSI values in the signatureArray
            for index in 0..<(signatureArray.count-1){
                RSSI_1 = RSSI_1 + signatureArray[index][0]
                RSSI_2 = RSSI_2 + signatureArray[index][1]
                RSSI_3 = RSSI_3 + signatureArray[index][2]
            }
            RSSI_1 = RSSI_1/Double(signatureArray.count-1)
            RSSI_2 = RSSI_2/Double(signatureArray.count-1)
            RSSI_3 = RSSI_3/Double(signatureArray.count-1)
            
            // Clear signatureArray to prepare for next collection
            signatureArray.removeAll()
            timerLabel.text = "Collection Complete"

        }
    }
}
