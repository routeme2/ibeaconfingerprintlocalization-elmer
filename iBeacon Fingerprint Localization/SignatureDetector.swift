//
//  SecondViewController.swift
//  iBeacon Fingerprint Localization
//
//  Created by Elmer Orellana on 4/25/17.
//  Copyright © 2017 RouteMe2. All rights reserved.
//

import UIKit
import CoreLocation
import Foundation

class SignatureDetector: UIViewController, CLLocationManagerDelegate {
    
    let iBeaconMajor:NSNumber = 9073              // All beacons share the same
    let iBeaconMinor_1:NSNumber = 1               // Each beacon has a
    let iBeaconMinor_2:NSNumber = 2               // unique minor
    let iBeaconMinor_3:NSNumber = 3
    var iBeaconRSSI_1 = 0.0                       // RSSI from iBeacon 1
    var iBeaconRSSI_2 = 0.0                       // RSSI from iBeacon 2
    var iBeaconRSSI_3 = 0.0                       // RSSI from iBeacon 3
    var signatureTable = [[Double]]()             // Will hold the entire database
    var myLatitude = 0.0                          // Will hold the latitude of eu
    var myLongitude = 0.0                         // Will hold the longitude of eu
    var GPSAccuracy = 0.0                       // Will hold the GPS horizontal accuracy
    var timer = Timer()                           // Create instance of timer
    var writeString:String = ""                   // Will hold the string that will be written to file
    var kNearest = 4                              // selects the number of nearest neighbors
    var coordinates = [0.0,0.0]                   // will store the approximated coordinates of the eu
    var reportName:String = ""                    // will be used as the report filename
    var anthonyReport:String = ""                 // report requested by Anthony
    var anthonyWriteString:String = ""            // writestring for Anthony's report
    var differencesTotal = 0.0                    // for Anthony's report
    
    
    // Select region to monitor our own iBeacons from kontakt.io
    // by using our UUID and identifing them as kontakt beacons
    // For monitoring a specific iBeacon include that iBeacon's major and minor
    
    let regionToMonitor = CLBeaconRegion(proximityUUID: UUID(uuidString:"F7826DA6-4FA2-4E98-8024-BC5B71E0893E")!, identifier: "kontakt beacons")
    
    // Declare a location Manager that coordinates all locations
    let locationManager = CLLocationManager()
    
    
    // Globle variable for file's name that will contain the data collected
    var fileName:String = "fingerprintDbiPhone"


    override func viewDidLoad() {
        super.viewDidLoad()
       
        startLocationManager() // This function sets iBeaconRSSI_1, iBeaconRSSI_2, and iBeaconRSSI_3
        
        signatureTable = parseDatabaseFile()    // Parses the file to break down the database file
        
        //NOTE: the findMyLocation() function gets called in the locationManager() function
        
        ///////////////////////////////// Tap anywhere to hide keyboard ////////////////////////////////////////////
        
        //Looks for single or multiple taps.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(DatabaseMaker.dismissKeyboard))
        
        //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
        tap.cancelsTouchesInView = false
        
        view.addGestureRecognizer(tap)
        
        //Create a file name that is unique
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "MMddyyyy-hhmmss"
        let dateStringFormat = dayTimePeriodFormatter.string(from: Date())
        
        reportName = "FingerprintingFieldTest\(dateStringFormat).txt"
        
        anthonyReport = "AnthonyReport\(dateStringFormat).txt"
    }
    
    // This function calculates the approximated coordinates of the eu
    func findMyLocation(){
        
        // Create an array that holds the differences between the real-time signature RSSI values
        // and the RSSI values of each RP in the database
        var rssiDifferencesArray = compareSignatures(array: signatureTable)
        

        var preCoordinates = [0.0,0.0]
        // Clear coordinates
        coordinates = [0.0,0.0]

        
            
            // Create a new array with the sum of the 0, 1, and 2 elements of the rssiDifferenceArray
            // append the angle and radius to index 3 and 4 of the
            var differencesSumArray = [[Double]]()
            
            // Calculate the sum of the differences of elements 0, 1, 2, and 3 in the rssiDifferenceArray
            for i in 0..<rssiDifferencesArray.count{
                // Add a row to the differenceSumArray
                differencesSumArray.append([])
                
                // Calculate the sum of the 0, 1, 2 elements of the rssiDifferenceArray
                let diffSum = rssiDifferencesArray[i][0] + rssiDifferencesArray[i][1] + rssiDifferencesArray[i][2]
                
                // append the difference, angle, and radius to the ith element in the differencesSumArray
                differencesSumArray[i] = [diffSum, rssiDifferencesArray[i][3], rssiDifferencesArray[i][4]]
            }
            
            //sort the differencesSumArray in ascending order
            differencesSumArray.sort{($0[0] < $1[0])}
        
        /////////////////////////////////// For Anthony's report /////////////////////////////////////////////
        
        for i in 0..<differencesSumArray.count{
            differencesTotal += differencesSumArray[i][0]
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        
        
        if differencesSumArray[0][0] == 0.0{
            preCoordinates[0] = differencesSumArray[0][1]
            preCoordinates[1] = differencesSumArray[0][2]
        }else{
            // Calculate the weighted average of the location
            
            var rpWeight = [Double]()
            
            // calculate the weight of the k-nearest neighbors
            if ((kNearestTextField.text!) != ""){
                kNearest = Int(kNearestTextField.text!)!
            }else{
                kNearest = 4
            }
            
            print(kNearest)
            for i in 0..<kNearest{
                
                // calculate individual weights and store in rpWeight array
                let weightCalculation = 1/differencesSumArray[i][0]
                rpWeight.append(weightCalculation)
            }
            
            // Calculate the total weight
            var totalWeight = 0.0
            for i in 0..<rpWeight.count{
                totalWeight += rpWeight[i]
            }
            
            // Calculate the weight percentage of the k-nearest RPs
            var weightPercent = [Double]()
            for i in 0..<rpWeight.count{
                weightPercent.append((rpWeight[i]/totalWeight))
            }
            
            //clear coordinates
            preCoordinates = [0.0,0.0]
            
            // Calculate the weighted average of the RP coordinates
            var sinAngleCalculation = 0.0
            var cosAngleCalculation = 0.0
            for i in 0..<weightPercent.count{
                print(differencesSumArray[i][1])
                
                sinAngleCalculation += weightPercent[i]*differencesSumArray[i][2]*sin((Double.pi/180.0)*differencesSumArray[i][1])
//                print("Weight percent: \(weightPercent[i])")
//                print("Sine Angle: \(sinAngleCalculation)")
                
                cosAngleCalculation += weightPercent[i]*differencesSumArray[i][2]*cos((Double.pi/180.0)*differencesSumArray[i][1])
//                print("Cosine Angle: \(sinAngleCalculation)")
                
                preCoordinates[1] += weightPercent[i]*differencesSumArray[i][2]
//                print("arctan: \(atan2(sinAngleCalculation, cosAngleCalculation))")
            }
            if sinAngleCalculation > 0.0 && cosAngleCalculation > 0.0{
                preCoordinates[0] = (180.0/Double.pi)*atan2(sinAngleCalculation, cosAngleCalculation)
            }else if cosAngleCalculation < 0.0{
                preCoordinates[0] = (180.0/Double.pi)*atan2(sinAngleCalculation, cosAngleCalculation)
            }else if sinAngleCalculation < 0.0 && cosAngleCalculation > 0.0{
                preCoordinates[0] = (180.0/Double.pi)*atan2(sinAngleCalculation, cosAngleCalculation) + 360
            }
        }

        coordinates = [preCoordinates[0], preCoordinates[1]]
        
        euCoordinates.text = ("(\(coordinates[0]) , \(coordinates[1]))")
        for i in 0..<kNearest{
            print(differencesSumArray[i])
        }
        print("Coordinates: (\(coordinates[0]), \(coordinates[1]))")
    }
    
    // This function takes the difference of each element of the database against the real-time
    // signature. It returns a new 2D array of the absolute value of the differences and 
    // their corresponding coordinates
    func compareSignatures(array: ([[Double]]))->([[Double]]){
        
        var returnArray = [[Double]]()
        
        // take the difference between the real-time signature and the RSSI values in each RP in 
        // the database
        for i in 0..<array.count{
            returnArray.append([])

            returnArray[i].append(abs(array[i][0] - iBeaconRSSI_1))        // Beacon 1 RSSI difference
            returnArray[i].append(abs(array[i][1] - iBeaconRSSI_2))        // Beacon 2 RSSI difference
            returnArray[i].append(abs(array[i][2] - iBeaconRSSI_3))        // Beacon 3 RSSI difference
            returnArray[i].append(array[i][3])                             // Angle
            returnArray[i].append(array[i][4])                             // Radius
            //returnArray[i].append(array[i][6])                           // Latitude
            //returnArray[i].append(array[i][7])                           // Longitude
        }
        
        return returnArray
    }
    
    
    // This function parses the database file and converts it into a 2D array of doubles containing
    // the elements of each RP vector
    func parseDatabaseFile()->([[Double]]){
        
        //////////////////// Read from database file //////////////////////////////////////////////
        
       // print("trying to read file")
        
        let fileURLProject = Bundle.main.path(forResource: fileName, ofType: "txt")
        
       // print("read fileName")
        
        var vectorTableStr = [[String]]()
        var vectorTableDouble = [[Double]]()
        
        do {
            let readString = try String(contentsOfFile: fileURLProject!, encoding: String.Encoding.utf8)

            print("about to separate by newline")
            
            let signaturesArray = readString.components(separatedBy: "\n")
            
            print(signaturesArray)
           
            print("about to separate by comma")
            
            print("signatureArray size is: \(signaturesArray.count)")
            
            for i in 0..<signaturesArray.count-1{
                
                print(i)
                
                vectorTableStr.append([])
                vectorTableDouble.append([])
            
                var signatureVector = signaturesArray[i].components(separatedBy: ",")
                //print("SignatureVector: \(signatureVector)")
                
                // Remove the \r end-of-line string
                signatureVector[signatureVector.count-1] = signatureVector[signatureVector.count-1].replacingOccurrences(of: "\r", with: "")

                vectorTableStr[i] = signatureVector
                
                //print("vectorTableStr: \(vectorTableStr)")
                
                // Number of 0.0 represent the size of the RP vectors
                if signatureVector.count == 6{
                    vectorTableDouble[i] = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
                }else{
                    vectorTableDouble[i] = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
                }
                //print("SignatureVector size: \(signatureVector.count)")
            
                for j in 0..<signatureVector.count{
                   // print(j)
                    vectorTableDouble[i][j] = Double(vectorTableStr[i][j])!
                 //   print(vectorTableDouble)
                }
            }
            
        }catch let error as NSError{
            print("Failed to read from project")
            print(error)
        }
        
        
        return vectorTableDouble
        /*****************************************************************************************/
    }
    
    
    func startLocationManager(){
        
        //////////////// locationManager delegate declaration and request for location authorization ///////////////
        
        
        // Let this class receive location updates
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        // Request authorization to use location services. Manually done in iOS settings.
        if (CLLocationManager.authorizationStatus() != CLAuthorizationStatus.authorizedWhenInUse){
            locationManager.requestWhenInUseAuthorization()
        }
        
        // Begin monitoring
        locationManager.startRangingBeacons(in: regionToMonitor)
        locationManager.startUpdatingLocation()
        
        /*****************************************************************************************/
    }
    
    
    /*********************************** Location Manager Function ********************************************/
    
    // Receive iBeacon updates on their current informatiom
    func locationManager(_ manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], in region: CLBeaconRegion) {
        
        // Grabs iBeacons' information
        for beacon in beacons {
            if beacon.major == iBeaconMajor {
                if beacon.minor == iBeaconMinor_1{
                    iBeaconRSSI_1 = Double(beacon.rssi)
                    if iBeaconRSSI_1 == 0.0{
                        iBeaconRSSI_1 = -100
                    }
                }
            }
            
            if beacon.major == iBeaconMajor {
                if beacon.minor == iBeaconMinor_2{
                    iBeaconRSSI_2 = Double(beacon.rssi)
                    if iBeaconRSSI_2 == 0.0{
                        iBeaconRSSI_2 = -100
                    }
                }
            }
            
            if beacon.major == iBeaconMajor {
                if beacon.minor == iBeaconMinor_3{
                    iBeaconRSSI_3 = Double(beacon.rssi)
                    if iBeaconRSSI_3 == 0.0{
                        iBeaconRSSI_3 = -100
                    }
                }
            }
        }
        
        //Display Beacon signature on iPhone screen
        iBeaconSignature.text = "[\(iBeaconRSSI_1), \(iBeaconRSSI_2), \(iBeaconRSSI_3), \(myLatitude), \(myLongitude)]"
        
        // Call the find my location function
        findMyLocation()
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations[0]
        
        // Get the latitude and longitude of eu
        myLatitude = location.coordinate.latitude
        myLongitude = location.coordinate.longitude
        GPSAccuracy = location.horizontalAccuracy
    }


//Calls this function when the tap is recognized.
func dismissKeyboard() {
    //Causes the view (or one of its embedded text fields) to resign the first responder status.
    view.endEditing(true)
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////



    /**********************************************************************************************************/
    
    @IBOutlet weak var iBeaconSignature: UILabel!
    @IBOutlet weak var euCoordinates: UILabel!
    @IBOutlet weak var distanceErrorLabel: UILabel!
    @IBOutlet weak var kNearestTextField: UITextField!
    @IBOutlet weak var xAngleTextField: UITextField!
    @IBOutlet weak var yRadiusTextField: UITextField!
    @IBAction func collectSample(_ sender: Any) {
        
        // Convert to Cartesian coordinates
        let x = coordinates[1]*cos((Double.pi/180.0)*coordinates[0])
        let y = coordinates[1]*sin((Double.pi/180.0)*coordinates[0])
        
        let angleRP:Double = Double(xAngleTextField.text!)!
        let radiusRP:Double = Double(yRadiusTextField.text!)!
        let xRP = radiusRP*cos((Double.pi/180.0)*angleRP)
        let yRP = radiusRP*sin((Double.pi/180.0)*angleRP)
        
        // calculate distance difference
        let deltaX = xRP - x
        let deltaY = yRP - y
        
        var distanceDifference = sqrt(deltaX*deltaX + deltaY*deltaY)
        distanceDifference = round(100.0*distanceDifference)/100.0

        
        // Prepare the information that will be written in the report text file
        let rTRSSITotal = iBeaconRSSI_1 + iBeaconRSSI_2 + iBeaconRSSI_3
        writeString = writeString + "\(coordinates[0]),\(coordinates[1]),\(xAngleTextField.text!),\(yRadiusTextField.text!),\(distanceDifference),\(rTRSSITotal)\n"
        
        //,\(myLatitude),\(myLongitude)\n"
        
        // Prepare information for Anthony's report
        // this will be written as a CSV .txt
        // Actual degrees,actual radius, estimated x,estimated y,total difference magnitude, RT magnitude sum
        
        
//        anthonyWriteString = anthonyWriteString + "\(xAngleTextField.text!),\(yRadiusTextField.text!),\(x),\(y),\(rTRSSITotal),\(myLatitude),\(myLongitude),\(GPSAccuracy)\n"
        
        // Clear differencesTotal
        differencesTotal = 0.0
        
        distanceErrorLabel.text = "Distance from actual: \(distanceDifference) meters"
    }
    
    
    @IBAction func generateReport(_ sender: Any) {
        // Stop looking for iBeacons
        locationManager.stopRangingBeacons(in: regionToMonitor)
        
        // Stop updating locations
        locationManager.stopUpdatingLocation()
        
        //Locate the document directory and save the data there
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            
            let dirFilePath = dir.appendingPathComponent(reportName)
            
            //Save the file
            do {
                try writeString.write(to: dirFilePath, atomically: false, encoding: String.Encoding.utf8)
                
                // Reports back to the tester immediate
                iBeaconSignature.text = "Report Created"
                
            } catch _ {
                
                // Reports back to the tester immediate
                iBeaconSignature.text = "Report Creation Failed"
                
                print("Could not save the file.")
            }
        }
        
//        // Generate Anthony's report
//        //Locate the document directory and save the data there
//        if let dir2 = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
//            
//            let dir2FilePath = dir2.appendingPathComponent(anthonyReport)
//            
//            //Save the file
//            do {
//                try anthonyWriteString.write(to: dir2FilePath, atomically: false, encoding: String.Encoding.utf8)
//                
//                // Reports back to the tester immediate
//                iBeaconSignature.text = "Anthony Report Created"
//                
//            } catch _ {
//                
//                // Reports back to the tester immediate
//                iBeaconSignature.text = "Anthony Report Creation Failed"
//                
//                print("Could not save the file.")
//            }
//        }
        
        // Clear the writeString and anthonyWriteString variable
        writeString = ""
        anthonyWriteString = ""
    }
}

